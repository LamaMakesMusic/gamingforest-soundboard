﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "New World Segment", menuName = "ScriptableObjects/Create World Segment", order = 1)]
public class WorldSegmentData : ScriptableObject
{
    public enum SegmentTypeOption
    {
        Ground = 0,
        Background = 1,
        Foreground = 2
    }

    public SegmentTypeOption SegmentType;
    public SegmentFloatRangeMap[] SegmentPrefabs;


    public GameObject GetSegmentByFloat(float value)
    {
        var map = SegmentPrefabs.FirstOrDefault(seg => seg.FloatRange.x <= value && seg.FloatRange.y >= value);
        if (map == default)
        {
            //TODO: Instead of random one, implement 'get closes to value'
            map = SegmentPrefabs[Random.Range(0, SegmentPrefabs.Length)];
        }

        return map?.Segment;
    }
}


[System.Serializable]
public class SegmentFloatRangeMap
{
    public GameObject Segment;
    public Vector2 FloatRange;
}
