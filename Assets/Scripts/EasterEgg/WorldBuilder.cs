﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldBuilder : MonoBehaviour
{
    public WorldSegmentData[] WorldSegments;

    [Header("World Limits")]
    public int PreloadedSegmentCount = 6;

    private List<WorldSegment> _activeSegments = new List<WorldSegment>();
    private List<WorldSegment> _inactiveSegments = new List<WorldSegment>();

    private GameManager _gameManager;
    private Plane[] _frustumPlanes;

    private bool _isSetUp = false;
    private bool _willCreateGap = false;
    private float _currentPerlinValue = 0f;

    private int _currentTileCountAfterGap = 0;

    private const float PERLIN_SCROLL_SPEED = .8f;


    private void Awake()
    {
        _gameManager = GetComponent<GameManager>();

        if (_gameManager == null)
        {
            Debug.LogError("GameManager Component not found!");
            this.enabled = false;
        }
    }
    private void Start()
    {
        _frustumPlanes = GeometryUtility.CalculateFrustumPlanes(_gameManager.MainCamera);

        _isSetUp = true;
    }

    public void MoveSegments(float speed)
    {
        if (!_isSetUp)
            return;

        if (_activeSegments.Count < PreloadedSegmentCount)
        {
            for (int i = _activeSegments.Count; i < PreloadedSegmentCount; i++)
                LoadNextSegment();
        }

        for (int i = _activeSegments.Count-1; i >= 0; i--)
        {
            _activeSegments[i].transform.Translate(Vector3.left * speed);

            if (IsSegmentOutOfSight(_activeSegments[i]))
            {
                _activeSegments[i].gameObject.SetActive(false);

                _activeSegments[i].CleanUp();

                _inactiveSegments.Add(_activeSegments[i]);
                _activeSegments.RemoveAt(i);
            }
        }
    }
    private void LoadNextSegment()
    {
        if (_gameManager.MinTilesAfterGapOverPlaytimeInMinutes.Evaluate(_gameManager.PlaytimeInMinutes) <= _currentTileCountAfterGap)
            _willCreateGap = _gameManager.GapChanceOverPlaytimeInMinutes.Evaluate(_gameManager.PlaytimeInMinutes) > Random.value;
        else
            _willCreateGap = false;

        //Segment
        WorldSegment segment = _inactiveSegments.FirstOrDefault();
        if (segment == default)
        {
            segment = new GameObject().AddComponent<WorldSegment>();
            segment.transform.parent = transform;
            segment.name = $"Segment_{_activeSegments.Count}";
        }

        _currentPerlinValue = Mathf.PerlinNoise(Time.time * PERLIN_SCROLL_SPEED, .5f);
        segment.gameObject.SetActive(true);
        segment.Populate(GetRandomSegmentData(WorldSegmentData.SegmentTypeOption.Ground, _currentPerlinValue));

        //Positioning
        if (_activeSegments.Count > 0)
        {
            var prevSegment = _activeSegments[_activeSegments.Count - 1];
            segment.transform.position = prevSegment.transform.position + Vector3.right * prevSegment.Collider.bounds.size.x;

            if (_willCreateGap)
            {
                segment.transform.position += Vector3.right * _gameManager.GapSizeOverPlaytimeInMinutes.Evaluate(_gameManager.PlaytimeInMinutes);
                _currentTileCountAfterGap = 0;
            }

            _currentTileCountAfterGap++;
        }
        else
            segment.transform.localPosition = Vector3.zero;

        
        //Lists
        _inactiveSegments.Remove(segment);
        _activeSegments.Add(segment);
    }
    private GameObject GetRandomSegmentData(WorldSegmentData.SegmentTypeOption segmentType, float perlinValue)
    {
        List<WorldSegmentData> allSegmentsOfType = new List<WorldSegmentData>();
        for (int i = 0; i < WorldSegments.Length; i++)
        {
            if (WorldSegments[i].SegmentType.Equals(segmentType))
                allSegmentsOfType.Add(WorldSegments[i]);
        }

        if (allSegmentsOfType.Count == 0)
        {
            Debug.LogError($"Could not find Segment of type {segmentType}");
            return null;
        }

        int randomSegmentIndex = Random.Range(0, allSegmentsOfType.Count);
        var segment = allSegmentsOfType.ElementAt(randomSegmentIndex)?.GetSegmentByFloat(perlinValue);

        return segment;
    }
    

    private bool IsSegmentOutOfSight(WorldSegment segment)
    {
        if (segment == null || segment.Collider == null)
        {
            Debug.LogError("Segment or Collider is null!", segment);
            return true;
        }

        //Checks only if out of sight on left-hand-side
        return !GeometryUtility.TestPlanesAABB(new[] { _frustumPlanes[0] }, segment.Collider.bounds);
    }
}


public class WorldSegment : MonoBehaviour
{
    public Collider2D Collider;

    public GameObject GroundSegment;
    public GameObject BackgroundSegment;
    //...

    public void Populate(GameObject ground)
    {
        GroundSegment = Instantiate(ground, transform);
        Collider = GroundSegment.GetComponent<Collider2D>();

        //BackgroundSegment = Instantiate(background, transform);
    }
    public void CleanUp()
    {
        Collider = null;

        Destroy(GroundSegment);
        Destroy(BackgroundSegment);
    }
}
