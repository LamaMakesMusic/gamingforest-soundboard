﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum ButtonTypeOption
    {
        ButtonA = 0,
        ButtonB = 1,
        Dialogue = 2
    }

    public GameObject PlayerPrefab;

    [Header("Start Values")]
    public int StartLifeCount;
    public float JumpForce;

    [Header("Balancing")]
    public AnimationCurve ScrollSpeedOverPlaytimeInMinutes;
    public AnimationCurve GapSizeOverPlaytimeInMinutes;
    public AnimationCurve GapChanceOverPlaytimeInMinutes;
    public AnimationCurve MinTilesAfterGapOverPlaytimeInMinutes;

    private int _livesLeft;
    private bool _isGameRunning = false;
    private bool _isGrounded = false;

    private Rigidbody2D _player;
    private Collider2D _playerCollider;

    private Camera _mainCamera;
    public Camera MainCamera 
    {   get 
        {
            if (_mainCamera == null)
                _mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

            return _mainCamera;
        }
    }

    private WorldBuilder _worldBuilder;
    private System.Diagnostics.Stopwatch _stopwatch;
    public float PlaytimeInMinutes 
    {
        get 
        {
            return msToMinutes(_stopwatch.ElapsedMilliseconds);
        }
    }

    private SortedList<ButtonTypeOption, bool> _buttonMapping = new SortedList<ButtonTypeOption, bool>();
    private List<ButtonTypeOption> _cleanUp = new List<ButtonTypeOption>();

    void Awake()
    {
        _stopwatch = new System.Diagnostics.Stopwatch();
        _worldBuilder = GetComponent<WorldBuilder>();

        foreach (ButtonTypeOption buttonType in System.Enum.GetValues(typeof(ButtonTypeOption)))
        {
            _buttonMapping.Add(buttonType, false);
        }
    }
    void Start()
    {
        StartGame();
    }

    void StartGame()
    {
        if (_player == null)
        {
            _player = Instantiate(PlayerPrefab, transform).GetComponent<Rigidbody2D>();
            _playerCollider = _player.GetComponent<Collider2D>();
        }

        _player.transform.position = Vector3.zero + Vector3.up;

        _livesLeft = StartLifeCount;
        _isGameRunning = true;

        _stopwatch.Start();
    }

    private void Update()
    {
        if (!_isGameRunning)
            return;

        _worldBuilder?.MoveSegments(ScrollSpeedOverPlaytimeInMinutes.Evaluate(PlaytimeInMinutes));
    }
    private void FixedUpdate()
    {
        _isGrounded = IsGrounded();

        if (_isGrounded)
        {
            if (_buttonMapping[ButtonTypeOption.ButtonA])
                Jump();
        }

    }
    private void LateUpdate()
    {
        for (int i = 0; i < _cleanUp.Count; i++)
            _buttonMapping[_cleanUp[i]] = false;

        _cleanUp.Clear();

        for (int i = 0; i < _buttonMapping.Count; i++)
        {
            if (_buttonMapping[_buttonMapping.Keys[i]])
                _cleanUp.Add(_buttonMapping.Keys[i]);
        }
    }

    private float msToMinutes(long milliseconds)
    {
        return (milliseconds * 0.0001f) / 6f;
    }

    public void SetButtonPressed(int index)
    {
        ButtonTypeOption button = (ButtonTypeOption) index;

        if (!_buttonMapping.ContainsKey(button))
        {
            Debug.LogError($"Button not found: '{nameof(button)}'");
            return;
        }

        _buttonMapping[button] = true;
    }
    public bool GetButton(ButtonTypeOption button)
    {
        return (_buttonMapping.ContainsKey(button) ? _buttonMapping[button] : false);
    }

    private bool IsGrounded()
    {
        return Physics2D.Raycast(_playerCollider.bounds.min, Vector2.down, .2f, 1 << 0);
    }

    private void Jump()
    {
        _player.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
    }
}
