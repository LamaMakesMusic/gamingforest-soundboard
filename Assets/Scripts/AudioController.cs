﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    public AudioMixer DefaultMixer;

    private AudioSource _AudioSource;

    private void Awake()
    {
        _AudioSource = GetComponent<AudioSource>();
    }


    public void PlayAudioClip(AudioClip clip)
    {
        if (!_AudioSource)
            return;

        _AudioSource.Stop();

        _AudioSource.clip = clip;

        _AudioSource.Play();
    }

    public void AdjustPitch(float pitch)
    {
        DefaultMixer.SetFloat("Pitch", pitch);
    }

}
