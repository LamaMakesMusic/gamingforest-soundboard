using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using System;
using TMPro;

public class UIController : MonoBehaviour
{
    [Header("Basic")]
    public GameObject CategorySlotPrefab;
    public GameObject SFXSlotPrefab;
    public Vector2 SlotSize;
    public Vector2 SlotPadding;

    public Image LoadingScreenBackground;
    public Image ProgressBarLogo;
    public Slider ProgressBar;
    public Image ProgressBarText;

    public Text PathToSFXFolderDisplay;

    [Header("Audio")]
    public RectTransform AudioSettingsRect;
    public Slider PitchSlider;
    public Text PitchDisplay;

    [Header("Animation")]
    public float OpenSettingsStepSize;

    [Header("Misc")]
    public Texture2D DefaultIcon;

    private AudioController _audioController;
    private ScreenOrientation orientation;
    private RectTransform _mainRect, _categoryRect, _mainScrollViewContent;
    private ScrollRect _mainScrollView;
    private List<Transform> _categorySlots = new List<Transform>();
    private List<List<UISlot>> _SFXSlots = new List<List<UISlot>>();
    private List<Texture2D> _loadedSprites = new List<Texture2D>();

    private Coroutine _openSettingsRoutine;
    private string _importPath;

    private Vector3 _cachedMousePosition;
    private float _cachedMouseStartTime;
    private float _audioSettingsScreenWidthThreshold;

    private bool _pitchSliderPressed = false;

    private const float MAIN_HOR_SCROLL_VELOCITY = 24f;
    private const float MAIN_SCROLLBAR_HEIGHT = 60f;
    private const float MIN_AUDIO_SETTINGS_DRAG_DISTANCE = 20f;
    private const string SOUNDS_DIRECTORY_NAME_ANDROID = "GF Sounds";
    private const string SOUNDS_DIRECTORY_NAME_WINDOWS = "Audio Files";


    void Awake()
    {
        LoadingScreenBackground?.gameObject.SetActive(true);

		_mainScrollView = GetComponentInChildren<ScrollRect>();
		_mainScrollViewContent = _mainScrollView.transform.Find("Viewport/Content")?.GetComponent<RectTransform>();

        _audioController = GetComponent<AudioController>();

        if (AudioSettingsRect?.gameObject != null)
            AudioSettingsRect.gameObject.SetActive(false);

        AdjustPitchSlider(1f);
    }
	void Start()
	{
        _importPath = Application.dataPath;

        if (Application.platform == RuntimePlatform.Android)
            _importPath = Path.Combine(GetAndroidMediaPath(), SOUNDS_DIRECTORY_NAME_ANDROID);
        else
            _importPath = Path.Combine(Directory.GetParent(Application.dataPath).FullName, SOUNDS_DIRECTORY_NAME_WINDOWS);
        
        if (_importPath.StartsWith("Error:"))
        {
            ShowFilesNotFoundScreen(_importPath);
            this.enabled = false;
            return;
        }

        if (!Directory.Exists(_importPath))
            Directory.CreateDirectory(_importPath);

        UpdateScreenSize();

        StartCoroutine(ImportAudioClips());
	}

    private void Update()
    {
        if (LoadingScreenBackground.enabled)
            return;

        if (Screen.orientation != orientation)
            UpdateScreenSize();

        MonitorUserInput();
    }

    private void MonitorUserInput()
    {
        if (Input.touches.Length == 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _cachedMousePosition = Input.mousePosition;
                _cachedMouseStartTime = Time.time;
                
                #region Audio Settings Panel
                if ((AudioSettingsRect.gameObject.activeSelf || Input.mousePosition.x > _audioSettingsScreenWidthThreshold))
                {
                    if (_openSettingsRoutine == null)
                        _openSettingsRoutine = StartCoroutine(AnimateAudioSettingsWindow());
                }
                #endregion
            }
            else if (Input.GetMouseButton(0))
            {
                if (Input.mousePosition != _cachedMousePosition)
                {
                    UpdateMainScrollViewPosition((Input.mousePosition - _cachedMousePosition));
                    _cachedMousePosition = Input.mousePosition;
                }
            }
        }
        else
        {
            int idx = 0;
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    #region Audio Settings Panel
                    if ((AudioSettingsRect.gameObject.activeSelf || touch.position.x > _audioSettingsScreenWidthThreshold))
                    {
                        if (_openSettingsRoutine == null)
                            _openSettingsRoutine = StartCoroutine(AnimateAudioSettingsWindow(idx));
                    }
                    #endregion
                }
                else if (touch.phase == TouchPhase.Moved)
                {
                    if (touch.deltaPosition != Vector2.zero)
                        UpdateMainScrollViewPosition(touch.deltaPosition);
                }

                idx++;
            }
        }
    }
    private IEnumerator AnimateAudioSettingsWindow(int touchIndex = -1)
    {
        float endPosition = Mathf.Abs(AudioSettingsRect.sizeDelta.x * 0.5f);
        float threshold = 0f;

        if (!AudioSettingsRect.gameObject.activeSelf)
            AudioSettingsRect.gameObject.SetActive(true);
        else if (_pitchSliderPressed)
            yield break;

        bool stillMoving = true;
        bool mobileInput = (touchIndex > -1 && touchIndex < Input.touchCount);

        float startPosition = mobileInput ? Input.touches[touchIndex].position.x : Input.mousePosition.x;

        do
        {
            if (mobileInput)
                stillMoving = (Input.touches[touchIndex].phase != TouchPhase.Canceled && Input.touches[touchIndex].phase != TouchPhase.Ended);
            else
                stillMoving = Input.GetMouseButton(0);

            if (Mathf.Abs(startPosition - (mobileInput ? Input.touches[touchIndex].position.x : Input.mousePosition.x)) < MIN_AUDIO_SETTINGS_DRAG_DISTANCE)
                yield return null;
            else
            {
                float currentPosition = (Screen.width - (mobileInput ? Input.touches[touchIndex].position.x : Input.mousePosition.x)) * -1f;
                currentPosition += AudioSettingsRect.sizeDelta.x * 0.5f;

                AudioSettingsRect.anchoredPosition = new Vector2(Mathf.Clamp(currentPosition, -endPosition, endPosition), AudioSettingsRect.anchoredPosition.y);
            }

            yield return null;
        }
        while (stillMoving);
        
        bool openAnimation = (AudioSettingsRect.anchoredPosition.x < threshold);
        bool closeAnimation = (AudioSettingsRect.anchoredPosition.x > threshold);

        if (openAnimation || !closeAnimation)
        {
            while (AudioSettingsRect.anchoredPosition.x > -endPosition)
            {
                AudioSettingsRect.anchoredPosition = (AudioSettingsRect.anchoredPosition - Vector2.right * OpenSettingsStepSize);
                yield return null;
            }

            AudioSettingsRect.anchoredPosition = (new Vector2(-endPosition, AudioSettingsRect.anchoredPosition.y));
        }
        else if (closeAnimation || !openAnimation)
        {
            while (AudioSettingsRect.anchoredPosition.x < endPosition)
            {
                AudioSettingsRect.anchoredPosition = (AudioSettingsRect.anchoredPosition + Vector2.right * OpenSettingsStepSize);
                yield return null;
            }

            AudioSettingsRect.anchoredPosition = (new Vector2(endPosition, AudioSettingsRect.anchoredPosition.y));

            AudioSettingsRect.gameObject.SetActive(false);
        }

        _openSettingsRoutine = null;
        yield return null;
    }
    private void UpdateMainScrollViewPosition(Vector2 direction)
    {
        _mainScrollView.velocity = direction.x * Vector2.right * MAIN_HOR_SCROLL_VELOCITY;
    }

    private void UpdateScreenSize()
	{
        orientation = Screen.orientation;

		_mainRect = _mainScrollView?.GetComponent<RectTransform>();

		_mainRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Screen.width);
		_mainRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height);

        //Scrollbar height
        _mainScrollView.transform.GetChild(1)?.GetComponent<RectTransform>()?.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, MAIN_SCROLLBAR_HEIGHT);

        _audioSettingsScreenWidthThreshold = Screen.width - 60f;
    }

    private IEnumerator ImportAudioClips()
    {
        yield return FadeLoadingScreen(true, 0f);

        DirectoryInfo dirInfo = new DirectoryInfo(_importPath);
        _SFXSlots.Clear();

        if (dirInfo.Exists)
        {
            #region Load Thumbnail Sprites
            _loadedSprites = new List<Texture2D>();
            var files = dirInfo.GetFiles("icon_*", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Extension.IndexOf(".png", StringComparison.OrdinalIgnoreCase) > -1 || files[i].Extension.IndexOf(".jpg", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    UpdateProgressBar(i / (float)files.Length);

                    yield return LoadThumbnails(files[i]);
                }
            }
            #endregion

            #region Load Audio Clips
            DirectoryInfo[] childDirs = dirInfo.GetDirectories("*", SearchOption.TopDirectoryOnly);

            if (childDirs?.Length > 0)
            {
                //For all child-directories
                for (int i = 0; i < childDirs.Length; i++)
                {
                    UpdateProgressBar(i / (float)childDirs.Length);

                    //Create Category for Folder
                    CreateCategorySlot(childDirs[i].Name);

                    //Load all audio files into category
                    yield return LoadAudioClips(childDirs[i], i);
                }
            }
            else
            {
                ShowFilesNotFoundScreen(_importPath);
            }
            #endregion

            LayoutSlots();
        }

        yield return FadeLoadingScreen(false, .5f);
    }

    private void CreateCategorySlot(string name)
    {
        _categorySlots.Add(Instantiate(CategorySlotPrefab, _mainScrollViewContent).transform);

        name = name.Replace("_", " ");

        _categorySlots[_categorySlots.Count - 1].name = name;
        _categorySlots[_categorySlots.Count - 1].Find("Header Panel/Header Text").GetComponent<TextMeshProUGUI>().text = name;

        RectTransform catSlotRT = _categorySlots[_categorySlots.Count - 1].GetComponent<RectTransform>();
        catSlotRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _mainRect.rect.width);
        catSlotRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Screen.height - MAIN_SCROLLBAR_HEIGHT);

        _SFXSlots.Add(new List<UISlot>());
    }

    private IEnumerator LoadAudioClips(DirectoryInfo dirInfo, int targetCategory)
    {
        AudioClip clip;
        UnityWebRequest uwr;
        FileInfo[] files = dirInfo.GetFiles("*", SearchOption.AllDirectories);

        for (int i = 0; i < files.Length; i++)
        {
            string filePath = $"file://{files[i].FullName}";

            if (files[i].Extension.Equals(".wav", StringComparison.OrdinalIgnoreCase))
                uwr = UnityWebRequestMultimedia.GetAudioClip(filePath, AudioType.WAV);
            else if (files[i].Extension.Equals(".mp3", StringComparison.OrdinalIgnoreCase))
                uwr = UnityWebRequestMultimedia.GetAudioClip(filePath, AudioType.MPEG);
            else if (files[i].Extension.Equals(".ogg", StringComparison.OrdinalIgnoreCase))
                uwr = UnityWebRequestMultimedia.GetAudioClip(filePath, AudioType.OGGVORBIS);
            else
                continue;

            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError)
            {
                Debug.LogWarning("Error loading file '%'\n".Replace("%", files[i].Name) + uwr.error);
                continue;
            }

            clip = DownloadHandlerAudioClip.GetContent(uwr);
            clip.name = files[i].Name;

            //Instantiate Slot for new AudioClip
            InstantiateSlot(clip, targetCategory);
        }

        yield return null;
    }
    private IEnumerator LoadThumbnails(FileInfo file)
    {
        var uwr = UnityWebRequestTexture.GetTexture($"file://{file.FullName}");

        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            Debug.LogWarning("Error loading file '%'\n".Replace("%", file.Name) + uwr.error);
            yield break;
        }

        var texture = DownloadHandlerTexture.GetContent(uwr);
        if (texture != null)
        {
            texture.name = file.Name.Substring(5); //Removes 'icon_'-prefix
            _loadedSprites.Add(texture);
        }

        yield return null;
    }

    private void InstantiateSlot(AudioClip clip, int category)
    {
        var slot = Instantiate(SFXSlotPrefab, _categorySlots[category].Find("Category Scroll View/Viewport/Content")).GetComponent<UISlot>();

        _SFXSlots[category].Add(slot);
        
        slot.UpdateSlotInfo(clip, SlotSize, GetThumbnailFromFileName(clip.name));


        Button b = _SFXSlots[category][_SFXSlots[category].Count - 1].GetComponent<Button>();

        AudioClip clipToPlay = _SFXSlots[category][_SFXSlots[category].Count - 1].SlotClip;

        b?.onClick.AddListener(delegate { _audioController?.PlayAudioClip(clipToPlay); });
    }

    private void LayoutSlots()
    {
        Vector2 categoryRectSize;
        RectTransform slot, categorySVContent;
        Vector3 currPos;

        for (int i = 0; i < _categorySlots.Count; i++)
        {
            categorySVContent = _categorySlots[i].Find("Category Scroll View/Viewport/Content").GetComponent<RectTransform>();
            categoryRectSize = Vector2.zero;
            currPos = Vector3.zero - Vector3.up * (SlotPadding.y + SlotSize.y * 0.5f);

            for (int j = 0; j < _SFXSlots[i].Count; j++)
            {
                slot = _SFXSlots[i][j].GetComponent<RectTransform>();

                currPos.x += SlotPadding.x + (SlotSize.x * 0.5f);

                slot.localPosition = currPos;

                currPos.x += (SlotSize.x * 0.5f);

                categoryRectSize.x = currPos.x;

                //IF no more space, next row
                if (currPos.x + SlotPadding.x*2 + SlotSize.x > _mainRect.rect.size.x)
                {
                    currPos.x = 0;
                    currPos.y -= (SlotPadding.y + SlotSize.y);

                    categoryRectSize.y = Mathf.Abs(currPos.y);
                }
            }

            categoryRectSize.x += SlotPadding.x + SlotSize.x * 0.5f;
            categoryRectSize.y += SlotPadding.y + SlotSize.y * 0.5f;

            categorySVContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, categoryRectSize.x);
            categorySVContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, categoryRectSize.y);
        }
    }

    private IEnumerator FadeLoadingScreen(bool fadeIn, float duration)
    {
        if (fadeIn)
            LoadingScreenBackground.enabled = true;
        else
        {
            ProgressBar.gameObject.SetActive(false);
            ProgressBarLogo.gameObject.SetActive(false);
        }

        LoadingScreenBackground.CrossFadeAlpha((fadeIn ? 1f : 0f), duration, true);

        if (duration > 0f)
            yield return new WaitForSecondsRealtime(duration);

        if (fadeIn)
        {
            ProgressBar.gameObject.SetActive(true);
            ProgressBarLogo.gameObject.SetActive(true);
        }
        else
            LoadingScreenBackground.enabled = false;

        yield return null;
    }
    private void UpdateProgressBar(float percentage)
    {
        ProgressBar.value = percentage;

        if (ProgressBarText != null)
            ProgressBarText.fillAmount = percentage;
    }

    private void ShowFilesNotFoundScreen(string path)
    {
        PathToSFXFolderDisplay.text = path;
        PathToSFXFolderDisplay.rectTransform.parent.gameObject.SetActive(true);
    }

    public void AdjustPitchSlider(float pitch)
    {
        pitch = Mathf.Round(pitch * 10f) / 10f;

        PitchSlider.value = pitch;
        PitchDisplay.text = "x " + pitch.ToString("F1");
        _audioController.AdjustPitch(pitch);
    }

    private string GetAndroidMediaPath()
    {
        string path = "";

        try {
            AndroidJavaClass env_class = new AndroidJavaClass("android.os.Environment");
            AndroidJavaObject extStorageDirFile = env_class.CallStatic<AndroidJavaObject>("getExternalStoragePublicDirectory", env_class.GetStatic<string>("DIRECTORY_MUSIC"));
            path = extStorageDirFile?.Call<string>("getAbsolutePath");

            if (string.IsNullOrWhiteSpace(path))
                path = "Error: External Storage Public Directory could not be located. Missing the necessary permissions?";
            else if (extStorageDirFile.Call<bool>("exists"))
                extStorageDirFile.Call<bool>("mkdirs");
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            path = $"Error: {e.ToString()}";
        }

        return path;
    }
    private Texture2D GetThumbnailFromFileName(string fileName)
    {
        string[] split = fileName.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        
        for (int i = 0; i < _loadedSprites?.Count; i++)
        {
            if (_loadedSprites[i].name.IndexOf(split[0], StringComparison.OrdinalIgnoreCase) > -1)
                return _loadedSprites[i];
        }

        return DefaultIcon;
    }

    public void SetPitchSliderPressed(bool isPressed)
    {
        _pitchSliderPressed = isPressed;
    }
}
	