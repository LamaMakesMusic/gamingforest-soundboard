﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISlot : MonoBehaviour
{
    public string SlotName;
    public AudioClip SlotClip;

    public void UpdateSlotInfo(AudioClip slotClip, Vector2 slotSize, Texture2D slotImage)
    {
        SlotClip = slotClip;

        string temp = System.IO.Path.GetFileNameWithoutExtension(SlotClip.name).Replace("_", " ");
        var strArray = temp.Split("-".ToCharArray());
        if (strArray?.Length > 1)
            temp = strArray[1];
        SlotName = temp;

        gameObject.name = SlotName;

        //UI Stuff
        transform.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = SlotName;

        RectTransform rt = transform.GetComponent<RectTransform>();
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, slotSize.x);
        rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, slotSize.y);

        if (slotImage != null)
        {
            var sprite = Sprite.Create(slotImage, new Rect(Vector2.zero, new Vector2(slotImage.width, slotImage.height)), Vector2.zero);
            sprite.name = slotImage.name;

            transform.GetComponentInChildren<Image>().sprite = sprite;
        }
    }

}
