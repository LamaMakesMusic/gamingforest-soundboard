﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAnimator : MonoBehaviour
{
    public bool AutoStart = true;
    public bool LoopAnimation = true;
    public float FramesPerSecond = 24f;
    [Space]
    public Image ImageComponent;
    public Sprite[] AnimationFrames;

    private Coroutine frameChangeRoutine;
    private int frameIndex;

    void OnEnable()
    {
        if (ImageComponent == null || AnimationFrames == null || AnimationFrames.Length == 0)
            this.enabled = false;

        if (AutoStart)
            StartAnimation();
    }
    private void OnDisable()
    {
        StopAnimation();
    }

    public void StartAnimation()
    {
        if (frameChangeRoutine == null)
            frameChangeRoutine = StartCoroutine(FrameChanger());
    }
    public void StopAnimation()
    {
        if (frameChangeRoutine != null)
        {
            StopCoroutine(frameChangeRoutine);
            frameChangeRoutine = null;
        }
    }

    private IEnumerator FrameChanger()
    {
        frameIndex = 0;
        WaitForSeconds wait = new WaitForSeconds(1/FramesPerSecond);

        while (this.enabled)
        {
            yield return wait;

            ImageComponent.sprite = AnimationFrames[frameIndex];

            if (frameIndex < AnimationFrames.Length - 1)
                frameIndex++;
            else if (LoopAnimation)
                frameIndex = 0;
            else
                break;
        }

        frameChangeRoutine = null;
        yield return null;
    }
}
